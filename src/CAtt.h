//
// Created by ADMIN on 9. 5. 2020.
//

#ifndef SEMESTRALKA_CATT_H
#define SEMESTRALKA_CATT_H

#include <string>
#include "CCard.h"
#include "CDef.h"

using namespace std;

class Att:public Card  {
protected:
    string Name;
    int position;
public:
    /**
    * @brief Construct a new Attackcard
    */
    Att(){};
/**
* @brief Construct a new Attack card
*/
    Att(int Hlt,int Att,int C,string N);
    /**
* @brief Card destructor
*/
    virtual ~Att();
    /**
 * @brief Card losses health
 */
   void LoseHP(int Dmg) override;
   /**
    * @brief Card "dies"
    */
    void Die(Att * C);
    /**
     * @brief Card attacks opposing side
     */
    virtual void Fight(Card);
    /**
    * @brief Card infomration printed
     * make change to fint board
    */
    void printInfo();

    /**
    * @brief Setters and getter for attack
    */
    string GetName()const ;
    void SetName(string N);
    int GetHealth()const ;
    void SetHealth(int number);
    int GetAttack()const;
    void SetAttack(int number);
    virtual void SetCost(int C);
    virtual int GetCost()const;

};


#endif //SEMESTRALKA_CATT_H
