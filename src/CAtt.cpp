//
// Created by ADMIN on 9. 5. 2020.
//

#include "CAtt.h"

Att::Att(int Hlt,int Att,int C,string N):Card(Hlt,Att,C),Name(N),position(0){}

void Att::LoseHP(int Dmg) {
    this->Health-=Dmg;
    if(Health<=0)Die(this);
}
void Att::Die(Att * C) {
    // this->Health=0;
    //  this->Attack=0;
}

void Att::Fight(Card enemy) {
    int enemyAttack=enemy.GetAttack();
    enemy.LoseHP(this->GetAttack());
    this->LoseHP(enemyAttack);
}

int Att::GetHealth() const{
    return this->Health;
}

int Att::GetAttack() const{
    return this->Attack;
}

void Att::SetHealth(int number) {
    this->Health=number;
}

void Att::SetAttack(int number) {
    this->Attack=number;
}

string Att::GetName()const {
    return this->Name;
}

void Att::SetName(string N) {
    this->Name=N;
}
void Att::SetCost(int C) {
    this->Cost=C;
}

int Att::GetCost() const{
    return this->Cost;
}

void Att::printInfo() {
    cout << this->Name;
    Card::printInfo();
}

Att::~Att() {}
