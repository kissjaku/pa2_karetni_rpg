//
// Created by ADMIN on 8. 5. 2020.
//

#ifndef SEMESTRALKA_IPLAYER_H
#define SEMESTRALKA_IPLAYER_H

#include "CHand.h"
#include "CMinions.h"

class PLayer {
protected:
    int healtBar;
    int manaCurrent;
    int manaMax;
public:
    /**
* @brief Contructor for PLayer
*/
    PLayer(){};
    /**
* @brief Contructor for PLayer
*/
    PLayer(int HP,int current,int max):healtBar(HP),manaCurrent(current),manaMax(max){}
    /**
* @brief Detructor for PLayer
*/
    ~PLayer(){}

    virtual /**
* @brief Increase in mana after each turn
*/
    void increaseMana(){manaCurrent++;};
    /**
* @brief Setters and getters
*/
    int GetHp()const {return this->healtBar;}
    int GetMana()const {return this->manaCurrent;}
    void SetHp(int Num){this->manaCurrent=Num;}
    void SetMana(int Num){this->manaCurrent=Num;}
};


#endif //SEMESTRALKA_IPLAYER_H
