//
// Created by ADMIN on 8. 5. 2020.
//

#include "CHand.h"
Hand::Hand() {}

Hand::Hand(Burn &one, Def &two, Def &three, Att &four):burn(one),def1(two),def2(three),att1(four),att2(),heal(),handState(1){}

Hand::Hand(Heal &one, Def &two, Def &three, Att &four):heal(one),def1(two),def2(three),att1(four),att2(),burn(),handState(2){}

Hand::Hand(Burn &one, Def &two, Att &three, Att &four):burn(one),def1(two),att1(three),att2(four),def2(),heal(),handState(3){}

Hand::Hand(Heal &one, Def &two, Att &three, Att &four):heal(one),def1(two),att1(three),att2(four),def2(),burn(),handState(4){}

void Hand::print(int number) {
    if(number == 1) {
        cout << "|" << burn.GetName() << "|" << def1.GetName() << "|" << def2.GetName() << "|" << att1.GetName()<< "|\n|Power:";
        cout << burn.GetPower()<< "        |Att:"<< def1.GetAttack()<< "          |Att:"<< def2.GetAttack()<< "          |Att:"<< att1.GetAttack()<< "          |\n";
        cout << "|Cost:"<< burn.GetCost()<< "         |Health:"<< def1.GetHealth()<< "       |Health:"<< def2.GetHealth()<< "       |Health:"<< att1.GetHealth()<< "       |\n";
        cout << "|"<< "               |Cost:"<< def1.GetCost()<< "         |Cost:"<< def2.GetCost()<< "         |Cost:"<< att1.GetCost()<< "         |\n";
    }
    if(number == 2) {
        cout << "|" << heal.GetName() << "|" << def1.GetName() << "|" << def2.GetName() << "|" << att1.GetName()<< "|\n|Power:";
        cout << heal.GetPower()<< "        |Att:"<< def1.GetAttack()<< "          |Att:"<< def2.GetAttack()<< "          |Att:"<< att1.GetAttack()<< "          |\n";
        cout << "|Cost:"<< heal.GetCost()<< "         |Health:"<< def1.GetHealth()<< "       |Health:"<< def2.GetHealth()<< "       |Health:"<< att1.GetHealth()<< "       |\n";
        cout << "|"<< "               |Cost:"<< def1.GetCost()<< "         |Cost:"<< def2.GetCost()<< "         |Cost:"<< att1.GetCost()<< "         |\n";
    }
    if(number == 3) {
        cout << "|" << burn.GetName() << "|" << def1.GetName() << "|" << att1.GetName() << "|" << att2.GetName()<< "|\n|Power:";
        cout << burn.GetPower()<< "        |Att:"<< def1.GetAttack()<< "          |Att:"<< att1.GetAttack()<< "          |Att:"<< att2.GetAttack()<< "          |\n";
        cout << "|Cost:"<< burn.GetCost()<< "         |Health:"<< def1.GetHealth()<< "       |Health:"<< att1.GetHealth()<< "       |Health:"<< att2.GetHealth()<< "       |\n";
        cout << "|"<< "               |Cost:"<< def1.GetCost()<< "         |Cost:"<< att1.GetCost()<< "         |Cost:"<< att2.GetCost()<< "         |\n";
    }
    if(number == 4) {
        cout << "|" << heal.GetName() << "|" << def1.GetName() << "|" << att1.GetName() << "|" << att2.GetName()<< "|\n|Power:";
        cout << heal.GetPower()<< "        |Att:"<< def1.GetAttack()<< "          |Att:"<< att1.GetAttack()<< "          |Att:"<< att2.GetAttack()<< "          |\n";
        cout << "|Cost:"<< heal.GetCost()<< "         |Health:"<< def1.GetHealth()<< "       |Health:"<< att1.GetHealth()<< "       |Health:"<< att2.GetHealth()<< "       |\n";
        cout << "|"<< "               |Cost:"<< def1.GetCost()<< "         |Cost:"<< att1.GetCost()<< "         |Cost:"<< att2.GetCost()<< "         |\n";
    }
}

void Hand::seeChoice(int number) {
    if(number == 1)
    {
        cout<<"1.)Name:"<<burn.GetName()<<"Cost:"<< burn.GetCost()<<"  Power:"<< burn.GetPower()<<endl;
        cout<<"2.)Name:"<<def1.GetName()<<"Cost:"<< def1.GetCost()<<"  Attack:"<< def1.GetAttack()<<"  Health:"<< def1.GetHealth()<<endl;
        cout<<"3.)Name:"<<def2.GetName()<<"Cost:"<< def2.GetCost()<<"  Attack:"<< def2.GetAttack()<<"  Health:"<< def2.GetHealth()<<endl;
        cout<<"4.)Name:"<<att1.GetName()<<"Cost:"<< att1.GetCost()<<"  Attack:"<< att1.GetAttack()<<"  Health:"<< att1.GetHealth()<<endl;
    }
    if(number == 2)
    {
        cout<<"1.)Name:"<<heal.GetName()<<"Cost:"<< heal.GetCost()<<"  Power:"<< heal.GetPower()<<endl;
        cout<<"2.)Name:"<<def1.GetName()<<"Cost:"<< def1.GetCost()<<"  Attack:"<< def1.GetAttack()<<"  Health:"<< def1.GetHealth()<<endl;
        cout<<"3.)Name:"<<def2.GetName()<<"Cost:"<< def2.GetCost()<<"  Attack:"<< def2.GetAttack()<<"  Health:"<< def2.GetHealth()<<endl;
        cout<<"4.)Name:"<<att1.GetName()<<"Cost:"<< att1.GetCost()<<"  Attack:"<< att1.GetAttack()<<"  Health:"<< att1.GetHealth()<<endl;
    }
    if(number == 3)
    {
        cout<<"1.)Name:"<<burn.GetName()<<"Cost:"<< burn.GetCost()<<"  Power:"<< burn.GetPower()<<endl;
        cout<<"2.)Name:"<<def1.GetName()<<"Cost:"<< def1.GetCost()<<"  Attack:"<< def1.GetAttack()<<"  Health:"<< def1.GetHealth()<<endl;
        cout<<"3.)Name:"<<att1.GetName()<<"Cost:"<< att1.GetCost()<<"  Attack:"<< att1.GetAttack()<<"  Health:"<< att1.GetHealth()<<endl;
        cout<<"4.)Name:"<<att2.GetName()<<"Cost:"<< att2.GetCost()<<"  Attack:"<< att2.GetAttack()<<"  Health:"<< att2.GetHealth()<<endl;
    }
    if(number == 4)
    {
        cout<<"1.)Name:"<<heal.GetName()<<"Cost:"<< heal.GetCost()<<"  Power:"<< heal.GetPower()<<endl;
        cout<<"2.)Name:"<<def1.GetName()<<"Cost:"<< def1.GetCost()<<"  Attack:"<< def1.GetAttack()<<"  Health:"<< def1.GetHealth()<<endl;
        cout<<"3.)Name:"<<att1.GetName()<<"Cost:"<< att1.GetCost()<<"  Attack:"<< att1.GetAttack()<<"  Health:"<< att1.GetHealth()<<endl;
        cout<<"4.)Name:"<<att2.GetName()<<"Cost:"<< att2.GetCost()<<"  Attack:"<< att2.GetAttack()<<"  Health:"<< att2.GetHealth()<<endl;
    }
}

void Hand::SetIDs(int number) {
    if(number == 1)
    {
        burn.SetID(1);
        def1.SetID(2);
        def2.SetID(3);
        att1.SetID(4);
    }
    if(number == 2)
    {
        heal.SetID(1);
        def1.SetID(2);
        def2.SetID(3);
        att1.SetID(4);
    }
    if(number == 3)
    {
        burn.SetID(1);
        def1.SetID(2);
        att1.SetID(3);
        att2.SetID(4);
    }
    if(number == 4)
    {
        heal.SetID(1);
        def1.SetID(2);
        att1.SetID(3);
        att2.SetID(4);
    }
}

Def Hand::GetDef1() {
    return this->def1;
}

Def Hand::Getdef2() {
    return this->def2;
}

Att Hand::Getatt1() {
    return this->att1;
}

Att Hand::Getatt2() {
    return this->att2;
}

Burn Hand::Getburn() {
    return this->burn;
}

Heal Hand::Getheal() {
    return this->heal;
}

