//
// Created by ADMIN on 8. 5. 2020.
//

#ifndef SEMESTRALKA_CHUMAN_H
#define SEMESTRALKA_CHUMAN_H

#include "IPLayer.h"

class Human: public PLayer {
    Hand startingHand;
public:
    /**
* @brief Contructor for Human
*/
    Human(){}
    /**
* @brief Contructor for Human
*/
    Human(const Hand& H,int hp,int mana,int max):startingHand(H),PLayer(hp,mana,max){}
    /**
* @brief Detructor for Human
*/
    ~Human(){}
    /**
* @brief Increase in mana after each turn
*/
    void increaseMana() override;
    Hand getHand()const{ return this->startingHand;}
    void chooseHand();
};


#endif //SEMESTRALKA_CHUMAN_H
