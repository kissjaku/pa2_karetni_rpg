//
// Created by ADMIN on 8. 5. 2020.
//

#ifndef SEMESTRALKA_CGAME_H
#define SEMESTRALKA_CGAME_H

#include <fstream>
#include "CBoard.h"

class CGame {
protected:
    Board state;
    std::fstream saveFile;
public:
    /**
* @brief Construct a new Game
*/
    CGame();
    /**
* @brief Construct a new Game
*/
    CGame(Board & st):state(st){}
    /**
* @brief Destructor a new Game
*/
    ~CGame(){};
    /**
* @brief saves game
*/
    void SaveGame();
    /**
* @brief Load
*/
    void LoadGame();
    /**
* @brief Gameloop for each round
*/
    void GameLoop();

};


#endif //SEstatic MESTRALKA_CGAME_H
