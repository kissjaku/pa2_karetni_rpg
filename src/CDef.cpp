//
// Created by ADMIN on 9. 5. 2020.
//

#include "CDef.h"


Def::Def(int Hlt,int Att,int C,string N):Card(Hlt,Att,C),Name(N),position(0){}

Def::~Def() = default;

void Def::LoseHP(int Dmg) {
    this->Health-=Dmg;
    if(Health<=0)Die(this);
}
void Def::Die(Def* C) {
    // this->Health=0;
    //  this->Attack=0;
}


int Def::GetHealth()const {
    return this->Health;
}

int Def::GetAttack()const {
    return this->Attack;
}

void Def::SetHealth(int number) {
    this->Health=number;
}

void Def::SetAttack(int number) {
    this->Attack=number;
}
string Def::GetName() const{
    return this->Name;
}

void Def::SetName(string N) {
    this->Name=N;
}
void Def::SetCost(int C) {
    this->Cost=C;
}

int Def::GetCost() const{
    return this->Cost;
}
void Def::printInfo() {
    cout << this->Name;
    Card::printInfo();
}
