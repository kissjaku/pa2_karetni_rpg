//
// Created by ADMIN on 8. 5. 2020.
//

#ifndef SEMESTRALKA_CBOT_H
#define SEMESTRALKA_CBOT_H

#include "IPLayer.h"
#include "IAi.h"
#include "CHand.h"
#include "CMinions.h"

template <class IAi> class Bot;

template <class IAi>
class Bot: public PLayer {
protected:
    Hand startingHand;
    Minions field;
public:
    /**
* @brief Ai for bot
*/
    IAi logic;
    /**
* @brief Contructor for Bot
*/
    Bot(){};
    /**
* @brief Contructor for Bot
*/
    Bot(Hand & H,int hp,int mana,int max):startingHand(H),PLayer(hp,mana,max),field(){}
    /**
* @brief Detructor for Bot
*/
    ~Bot(){}
    /**
* @brief Increase in mana after each turn
*/
    void increaseMana(){};
};


#endif //SEMESTRALKA_CBOT_H
