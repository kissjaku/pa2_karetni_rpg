//
// Created by ADMIN on 8. 5. 2020.
//

#ifndef SEMESTRALKA_CCARD_H
#define SEMESTRALKA_CCARD_H


#include <iostream>

class Card {
protected:
    int Health;
    int Attack;
    int Cost;
    int ID;
public:
    /**
   * @brief Construct a new Card
   */
    Card();
    /**
* @brief Construct a new Card (spell)
*/
    Card(int Hlt,int Att,int C);
    /**
* @brief Construct a new Card
*/
    explicit Card(int cost):Cost(cost),Health(0),Attack(0),ID(0){};
    /**
* @brief Destructor  Card
*/
    virtual ~Card();
    /**
* @brief Card to loses HP
*/
    virtual void LoseHP(int Dmg);
    /**
* @brief Card to Die on board
*/
    virtual void Die(Card * C);
    /**
* @brief Card to Attack enemy
*/
    virtual void Fight(Card enemy);
    /**
* @brief Card infomration printed
*/
    virtual void printInfo()const;
    /**
     * @brief Setters and getter for Card
     */
    virtual void SetCost(int C);
    virtual int GetCost()const;
    virtual int GetHealth()const;

    virtual void SetHealth(int number);
    virtual int GetAttack()const;

    virtual void SetAttack(int number);
    virtual void SetID(int num){this->ID=num;};

};


#endif //SEMESTRALKA_CCARD_H
