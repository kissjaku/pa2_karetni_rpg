//
// Created by ADMIN on 8. 5. 2020.
//

#ifndef SEMESTRALKA_CAISIMPLE_H
#define SEMESTRALKA_CAISIMPLE_H

#include "IAi.h"
#include "CHand.h"
#include "CBot.h"

class CAiSimple:public IAi  {
    friend class Bot<CAiSimple>;
    protected:
    /**
* @brief Just basic playing of card
*/
    virtual void Play(Hand & H){};
};


#endif //SEMESTRALKA_CAISIMPLE_H
