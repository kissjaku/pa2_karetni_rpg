//
// Created by ADMIN on 9. 5. 2020.
//

#ifndef SEMESTRALKA_CDEF_H
#define SEMESTRALKA_CDEF_H

#include <string>
#include "CCard.h"
#include "CAtt.h"
using namespace std;

class Def:public Card {
protected:
    string Name;
    int position;
public:
    /**
    * @brief Construct a new Defensive card
    */
    Def(){};
    /**
    * @brief Construct a new Defensive card
    */
    Def(int Hlt,int Att,int C,string N);
    /**
   * @brief Card destructor
   */
    virtual ~Def();
    /**
 * @brief Card losses health
 */
    void LoseHP(int Dmg) override;
    /**
 * @brief Card "dies"
 */
    void Die(Def * C);
    /**
* @brief Card infomration printed
*/
//todo change format to fit board(temporar form of print)
    void printInfo();
    /**
    * @brief Setters and getter for Def
    */
    string GetName()const;
    void SetName(string N);
    int GetHealth()const;
    void SetHealth(int number);
    int GetAttack() const;
    void SetAttack(int number);
    virtual void SetCost(int C);
    virtual int GetCost()const;
};


#endif //SEMESTRALKA_CDEF_H
