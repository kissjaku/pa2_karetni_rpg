//
// Created by ADMIN on 8. 5. 2020.
//

#ifndef SEMESTRALKA_CHAND_H
#define SEMESTRALKA_CHAND_H

#include<iostream>
using namespace std;

#include "CCard.h"
#include "CDef.h"
#include "CAtt.h"
#include "CBurn.h"
#include "CHeal.h"

//todo make this template
class Hand {
private:
    int handState;
public:
    /**
* @brief public for now for testing purposes
*/
    Def def1;
    Def def2;
    Att att1;
    Att att2;
    Burn burn;
    Heal heal;
    /**
 * @brief Construtors for Hand
 */
    Hand();
    Hand(Burn &one,Def &two,Def &three,Att &four);
    Hand(Heal &one,Def &two,Def &three,Att &four);
    Hand(Burn &one,Def &two,Att &three,Att &four);
    Hand(Heal &one,Def &two,Att &three,Att &four);
    void print(int number);
    void seeChoice(int number);
    int getState()const{ return this->handState;}
    void setState(int num){this->handState=num;}
    void SetIDs(int number);
    Def GetDef1();
    Def Getdef2();
    Att Getatt1();
    Att Getatt2();
    Burn Getburn();
    Heal Getheal();
};


#endif //SEMESTRALKA_CHAND_H
