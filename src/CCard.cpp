//
// Created by ADMIN on 8. 5. 2020.
//

#include "CCard.h"

Card::Card() = default;

Card::Card(int Hlt, int Att,int C):Health(Hlt),Attack(Att),Cost(C),ID(0){}

Card::~Card() = default;

void Card::LoseHP(int Dmg) {
    this->Health-=Dmg;
    if(Health<=0)Die(this);
}
void Card::Die(Card * C) {
   // this->Health=0;
  //  this->Attack=0;
}

void Card::Fight(Card enemy) {
    int enemyAttack=enemy.GetAttack();
    enemy.LoseHP(this->GetAttack());
    this->LoseHP(enemyAttack);
}

int Card::GetHealth() const{
    return this->Health;
}

int Card::GetAttack() const{
    return this->Attack;
}

void Card::SetHealth(int number) {
    this->Health=number;
}

void Card::SetAttack(int number) {
    this->Attack=number;
}
 void Card::SetCost(int C) {
    this->Cost=C;
}

int Card::GetCost() const{
    return this->Cost;
}

void Card::printInfo()const{
    std::cout<<"  HP:"<<this->Health<<"  Att:"<<this->Attack<<std::endl;
}

