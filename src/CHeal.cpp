//
// Created by ADMIN on 10. 5. 2020.
//

#include "CHeal.h"
string Heal::GetName() const{
    return this->Name;
}

void Heal::SetName(char *N) {
    this->Name=N;
}
void Heal::SetCost(int C) {
    this->Cost=C;
}

int Heal::GetCost() const{
    return this->Cost;
}

void Heal::DoHeal(Card &a) {
    Spell::DoHeal(a);
}

void Heal::printSpell() const {
    cout<<this->Name<<"  Cost:"<<this->Cost;
    Spell::printSpell();
}
