//
// Created by ADMIN on 9. 5. 2020.
//

#ifndef SEMESTRALKA_CMINIONS_H
#define SEMESTRALKA_CMINIONS_H

#include "CDef.h"
#include "CAtt.h"

class Minions {
protected:
    Def firstD;
    Def secondD;
    Att firstA;
    Att secondA;
public:
    /**
* @brief Contructor for Minions on battle field
*/
    Minions();
/**
* @brief Detructor for Minions
*/
~Minions();
/**
* @brief Setters getter for playning card onto field
*/
    Def GetFirstD()const;
    void SetFirstD(const Def& a);
    Def GetSecondD()const;
    void SetSecondD(const Def& a);
    Att GetFirstA()const;
    void SetFirstA(const Att& a);
    Att GetSecondA()const;
    void SetSecondA(const Att& a);
    /**
* @brief prints for minion state of each player
*/
    void print1();
    void print2();
};


#endif //SEMESTRALKA_CMINIONS_H
