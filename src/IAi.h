//
// Created by ADMIN on 8. 5. 2020.
//

#ifndef SEMESTRALKA_IAI_H
#define SEMESTRALKA_IAI_H


class IAi {
public:
    /**
 * @brief Const for Ai
 */
    IAi(){};
    /**
    * @brief Function of all Inteligences
     */
    virtual void Solve(){};
    /**
  * @brief Function of all Inteligences
   */
    virtual void Play(){};
};


#endif //SEMESTRALKA_IAI_H
