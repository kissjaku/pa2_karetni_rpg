#include <iostream>
#include <stdlib.h>
#include <cctype>
#include <cassert>
#include <cstring>
#include <iostream>
#include <iomanip>
#include <set>
#include <list>
#include <forward_list>
#include <map>
#include <vector>
#include <queue>
#include <string>
#include <deque>
#include <optional>
#include <variant>
#include <any>
#include <algorithm>
#include <functional>
#include <memory>
#include <stdexcept>
#include <synchapi.h>
using namespace std;

#include "CBoard.h"
#include "CAiHard.h"
#include "CAiSimple.h"
#include "CGame.h"


int main() {
    Hand p1;
    Hand p2;
    Human one(p1,30,3,20);
    Human two(p2,30,3,20);
    Bot<CAiSimple> enemy(p2,30,3,20);
   //todo TAKE THIS TO CGAME
    //Mock up for how game start off
    std::cout << "\n*------------------------------------------*\n"
                 "|                                          |\n"
                 "|                                          |\n"
                 "|            Welcome to my game            |\n"
                 "|                                          |\n"
                 "|                                          |\n"
                 "*------------------------------------------*" << std::endl;
    Sleep(1500);
    std::cout << "To start oFF i will epxlain few rules:\n"
                 "1.You will battle against your oppened unlti one of you losess all your HP or run out turns and game ends\n"<<endl;
    Sleep(1500);
    std::cout <<"2.At the start you both get 3 mana to spend on your cards that increases on end of each of your turn\n"<<endl;
    Sleep(1000);
    std::cout <<"3.You can choose 1 Spell card and 3 Minion cards to build your deck.\n";
    string gamemode;
    std::cout <<"\nPlease choose gamemode:\n"
                "1.) for SingPlayer\n"
                "2.) for 1v1"<<endl;
    cin>>gamemode;
    while(gamemode!="1" && gamemode!="2")
    {
        std::cout <<"Wrong input , please choose again:\n"
                    "1 for SingPlayer\n"
                    "2 for 1v1"<<endl;
        cin>>gamemode;
    }
    cout<<"\n\n\n------------------------------Player 1 please choose card------------------------------"<<endl;
    one.chooseHand();
    cout<<"\n\n\n------------------------------Player 2 please choose card------------------------------"<<endl;
    two.chooseHand();
    //todo TAKE THIS TO BOARD
    //Mock up for how board will look
    Board first(one,two);
    first.printBoardStatePvP(first.getPlayer1(),first.getPlayer2());
    //testing if field changes for player2 now only
    first.Play(2);
    first.Play(2);
    first.printBoardStatePvP(first.getPlayer1(),first.getPlayer2());
    return 0;
}