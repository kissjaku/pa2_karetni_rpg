//
// Created by ADMIN on 9. 5. 2020.
//

#include "CSpell.h"

void Spell::DoSpell(Card &a) {
    int curentHp=a.GetHealth();
    curentHp-=GetPower();
    a.SetHealth(curentHp);
}

void Spell::DoHeal(Card &a) {
    int curentHp=a.GetHealth();
    curentHp+=GetPower();
    a.SetHealth(curentHp);
}

void Spell::printSpell() const {
    cout << "  Power:" << this->power<<endl;
}

