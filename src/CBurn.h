//
// Created by ADMIN on 9. 5. 2020.
//

#ifndef SEMESTRALKA_CBURN_H
#define SEMESTRALKA_CBURN_H

#include "CSpell.h"

class Burn:public Spell {
protected:
    string Name;
public:
    /**
* @brief Construct a new Burn card
*/
    Burn() {}
    /**
    * @brief Construct a new Burn card
    */
    Burn(int cost,int power, string n) : Spell(cost,power), Name(n) {}
    /**
  * @brief Card infomration printed
  */
//todo change format to fit board(temporar form of print)
    void printSpell()const override;
    /**
  * @brief Similiar to Fight from CAtt
  */
    void DoSpell(Card &a) override;
    /**
     * @brief Setters and getter for Burn
     */
    string GetName() const;

    void SetName(char *N);

    void SetCost(int C) override;

    int GetCost() const override;
};

#endif //SEMESTRALKA_CBURN_H
