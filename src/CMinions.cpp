//
// Created by ADMIN on 9. 5. 2020.
//

#include "CMinions.h"

Minions::Minions():firstD(0,0,0,"Empty          "),secondD(0,0,0,"Empty          "),firstA(0,0,0,"Empty          "),secondA(0,0,0,"Empty          "){}

Minions::~Minions() {}

Def Minions::GetFirstD() const {
    return this->firstD;
}

void Minions::SetFirstD(const Def& a) {
    this->firstD.SetName(a.GetName());
    this->firstD.SetAttack(a.GetAttack());
    this->firstD.SetHealth(a.GetHealth());
}

Def Minions::GetSecondD() const {
    return this->secondD;
}

void Minions::SetSecondD(const Def& a) {
    this->secondD.SetName(a.GetName());
    this->secondD.SetAttack(a.GetAttack());
    this->secondD.SetHealth(a.GetHealth());
}

Att Minions::GetFirstA() const {
    return this->firstA;

}

void Minions::SetFirstA(const Att& a) {
    this->firstA=a;
    this->firstA.SetName(a.GetName());
    this->firstA.SetAttack(a.GetAttack());
    this->firstA.SetHealth(a.GetHealth());
}

Att Minions::GetSecondA() const {
    return this->secondA;
}

void Minions::SetSecondA(const Att& a) {
    this->secondA.SetName(a.GetName());
    this->secondA.SetAttack(a.GetAttack());
    this->secondA.SetHealth(a.GetHealth());
}

void Minions::print1() {
    cout << "|" << firstD.GetName() << "|" << secondD.GetName() << "|" << firstA.GetName() << "|" << secondA.GetName()<< "|\n|Att:";
    cout << firstD.GetAttack()<< "          |Att:"<< secondD.GetAttack()<< "          |Att:"<< firstA.GetAttack()<< "          |Att:"<< secondA.GetAttack()<< "          |\n";
    cout << "|Health:"<< firstD.GetCost()<< "       |Health:"<< secondD.GetHealth()<< "       |Health:"<< firstA.GetHealth()<< "       |Health:"<< secondA.GetHealth()<< "       |\n";
}
void Minions::print2() {
    cout << "|"   << firstA.GetName() << "|" << secondA.GetName()<< "|"<< firstD.GetName() << "|" << secondD.GetName()<< "|\n|Att:";
    cout << firstA.GetAttack()<< "          |Att:"<< secondA.GetAttack()<< "          |Att:"<<firstD.GetAttack() << "          |Att:"<< secondD.GetAttack()<< "          |\n";
    cout << "|Health:"<< firstA.GetHealth()<< "       |Health:"<< secondA.GetHealth()<<"       |Health:"        << firstD.GetHealth()<< "       |Health:"<< secondD.GetHealth()<< "       |\n";
}

