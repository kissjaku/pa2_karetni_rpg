//
// Created by ADMIN on 8. 5. 2020.
//

#ifndef SEMESTRALKA_CAIHARD_H
#define SEMESTRALKA_CAIHARD_H

#include "IAi.h"
#include "CHand.h"
#include "CHuman.h"
#include "CBot.h"

class CAiHard:public IAi {
        friend class Bot<CAiHard>;

protected:
    /**
* @brief Ai to play card
*/
    int Play(Hand & H);
    /**
* @brief Brains behind Play
*/
    int Solve(Hand & H,Human & A);

};


#endif //SEMESTRALKA_CAIHARD_H
