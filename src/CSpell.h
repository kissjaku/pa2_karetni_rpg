//
// Created by ADMIN on 9. 5. 2020.
//

#ifndef SEMESTRALKA_CSPELL_H
#define SEMESTRALKA_CSPELL_H


#include <string>
#include "CCard.h"


using namespace std;

class Spell:public Card {
protected:
    int power;
public:
    /**
* @brief Construct a new Spells card
*/
    Spell(){}
    /**
* @brief Construct a new Spells card
*/
    explicit Spell(int cost,int strenght): Card(cost),power(strenght){}
    /**
* @brief Destruct a new Spells card
*/
    ~Spell(){};
    /**
* @brief Card infomration printed
*/
    virtual void printSpell()const;
    /**
* @brief Main use of spells
*/
    virtual void DoSpell(Card &a);
    /**
* @brief Main use of spells
*/
    virtual void DoHeal(Card &a);
    /**
 * @brief Setters and getter for Burn
 */
    virtual void SetCost(int C){this->Cost=C;};
    virtual int GetCost()const{return this->Cost;};
    virtual void SetPower(int C){this->power=C;};
    virtual int GetPower()const{return this->power;};
};

#endif //SEMESTRALKA_CSPELL_H
