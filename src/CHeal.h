//
// Created by ADMIN on 10. 5. 2020.
//

#ifndef SEMESTRALKA_CHEAL_H
#define SEMESTRALKA_CHEAL_H


#include "CSpell.h"

class Heal:public Spell {
protected:
    string Name;
public:
    /**
* @brief Construct a new Heal card
*/
    Heal() {}
    /**
* @brief Construct a new Heal card
*/
    Heal(int cost,int power, string n) : Spell(cost,power), Name(n) {}
    /**
  * @brief Card infomration printed
  */
    void printSpell()const override;
    /**
  * @brief Main effect
  */
    void DoHeal(Card &a) override;
    /**
     * @brief Setters and getter for Heal
     */
    string GetName() const;

    void SetName(char *N);

    void SetCost(int C) override;

    int GetCost() const override;
};


#endif //SEMESTRALKA_CHEAL_H
