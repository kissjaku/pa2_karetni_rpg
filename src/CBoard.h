//
// Created by ADMIN on 8. 5. 2020.
//

#ifndef SEMESTRALKA_CBOARD_H
#define SEMESTRALKA_CBOARD_H

#include "CBot.h"
#include "CHuman.h"
#include "IAi.h"

class Board {
protected:
    Human player1;
    Human player2;
    Bot<IAi> bot;
    int turnCounter;
    bool turnControl;

public:
    Minions field1;
    Minions field2;
    /**
* @brief Construtors for Board
*/
    Board();
    /**
* @brief Construtors for Board
*/
    Board(const Human & one,const Human &two):player1(one),player2(two),turnCounter(1),turnControl(true),field1(),field2(){}
    /**
* @brief Construtors for Board
*/
    Board(const Human &one,const Bot<class IAi> &two):player1(one),bot(two),turnCounter(1),turnControl(false),field1(),field2(){}
    /**
* @brief Destrutors for Board
*/
    ~Board();
    /**
* @brief Function to endEach player turn.
*/
    void EndTurn();
    /**
* @brief Function to anounce winner of the game
*/
    void Vitcory(Human & winner);
    /**
* @brief Print for state of board in PVP
*/
    void printBoardStatePvP(Human p1,Human  p2);
    /**
* @brief Print for state of board in single player
*/
    void printBoardStateBot(Human &p,Bot<class IAi> & b);
    /**
* @brief Funcion for playing for each player
*/
    void Play(int turnCounter);

    Human getPlayer1(){return this->player1;}
    Human getPlayer2(){return this->player2;}
    Minions getField1(){return this->field1;}
    Minions getField2(){return this->field2;}
};


#endif //SEMESTRALKA_CBOARD_H
