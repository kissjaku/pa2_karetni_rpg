//
// Created by ADMIN on 9. 5. 2020.
//

#include "CBurn.h"
string Burn::GetName() const{
    return this->Name;
}

void Burn::SetName(char *N) {
    this->Name=N;
}
void Burn::SetCost(int C) {
    this->Cost=C;
}

int Burn::GetCost() const{
    return this->Cost;
}

void Burn::DoSpell(Card &a) {
    Spell::DoSpell(a);
}

void Burn::printSpell() const {
    cout<<this->Name<<"  Cost:"<<this->Cost;
    Spell::printSpell();
}
