## Concept

This project outcome will be a turn based Card RPG. 
The main features of this game can be found below. My game wil be based in the console. I based the concept on Hearthstone with board representation and game play with Stromboud where player alway have full hand after each turn.[https://play.google.com/store/apps/details?id=com.kongregate.mobile.stormbound.google&hl=sk](url)

It is a basic Card RPG. Player can choose to play solo or with friend.Each player will be given option to build thier own deck .From set of card you can choose 1 spell and 3 distict minions defensive or attack. Ultimate goal is to kill Enemy player be it friend or bot. Bot could have been given unfair advantage of seeing what you have. Soo i choose option where both players see each other cards.

Each player will start with predeterminet amount of mana wich he can spend on cards.On start of his turn he gains +1 max mana . Becaus you only have 4 cards to spend your mana on you have to choose carfuly what card you pick at the start because you can only play each card once per turn . This factor make choosing cheap but fast cards advantage early but player with more cost heavy cards will run the late game becaus max ammount of mana you can have is almost infinit.

This game is turn-based.  Player can play different att.Minions,deff.Minions,Spells. Each minion type has it own zone . Att minions attack atomaticly after each of our turns.Def minion are more cost efficien but have drawback of not being able to do much but defend their masters. Spell are your utility type where they help make tactical plays.

The Game can end on one of 3 options.You or your enemy lose all your HP . Game run out off turns and game will designed winner based on who has more HP.

During games player can save their progresss to file for later play . However if file is matipulated it could corrupt the savefile.

Initially the board will be very basic to accomade the small scale of this project if i choose to expand the card and the boeard it can all be done later.

## Main features

- Save/Load of a current state of the game
- Player can freely choose their own deck from predetermited set of cards
- Two game modes
- Basic and Advanced AI for Single player games
- Automatic minion attacks


## Polymorphism

I use polymorphism in 2 places . In cards and In Player/Bot.
I made diagram for Polymorphism that explains it better (see Diagram.png).